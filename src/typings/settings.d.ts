// 设备预设字段相关声明
declare namespace Device {
  type DeviceInfo = {
    id: number // 设备id
    type: string // 设备类型
  }

  type DeviceDeleteInfo = {
    id: number // 设备id
  }

  type DeviceSettings = {
    type: string
    parent_id: number
    value: string
    remark: string
    data_format: string
  }

  type DeviceSettingsEdit = {
    id: number
    type: string
    parent_id: number
    value: string
    remark: string
    data_format: string
  }
}
