// 设备预设字段 - 接口
import { ResPage } from '@/api/interface/index'
import http from '@/api'

// 根据分类获取配置列表
export const getDeviceList = (params: { page: number; page_size: number; type: string }) => {
  return http.get<ResPage<Device.DeviceInfo>>(`device/list`, params)
}

// 删除配置项值
export const deleteDevice = (params: { id: number }) => {
  console.log(params)
  return http.delete<Device.DeviceInfo>(`device/value`, params)
}

//新增配置项值
export const addDevice = (params: Device.DeviceInfo) => {
  return http.post<Device.DeviceSettings>(`device/value`, params)
}

//修改配置项值
export const editDevice = (params: Device.DeviceSettingsEdit) => {
  return http.put<Device.DeviceSettingsEdit>(`device/value`, params)
}
